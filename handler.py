import json
from os import environ
import requests
from requests.auth import HTTPBasicAuth
from loguru import logger
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES
import base64

# Environments
AES_MODE = eval(environ.get('AES_MODE'))
KEY_DECODE = base64.b64decode(environ.get('KEY'))
IV_DECODE = base64.b64decode(environ.get('IV'))
USER_EMAIL = environ.get('USER_EMAIL')
USER_TOKEN = environ.get('USER_TOKEN')
ClientID = environ.get('CLIENT_ID')
ClientSecret = environ.get('CLIENT_SECRET')

# Endpoints
environment = environ.get('DEPLOYMENT_ENV')
endpoints = {
    "development":{
        "server_bizagi": 'https://test-bizagi.alignetsac.com',
        "server_jira": 'https://alignet.atlassian.net'
    },
	"production": {
        "server_bizagi": '',
        "server_jira": 'https://alignet.atlassian.net'
	}
}

# Constantes
_CONTENT_TYPE = 'application/json'
_XPATH_CODE_PARTICIPANT = 'ListadeParticipantes.sCodigoParticipante'
_XPATH_CODE_PROJECT = 'ListadeProyectos.sIddeproyecto'
_NEXT_LINK = '@odata.nextLink'
_SIZE = '$top'
_SKIP = '$skip'

def get_prepared_body(action, msg, success, data=[], errors={}, code='01'):
    payload = {
        "action": action,
        "success": success,
        "data": data,
        "errors": errors,
        "meta": {
            "status": {
                "code": code,
                "message_ilgn": [{
                    "locale": "es_PE",
                    "value": msg
                }]
            }
        }
    }
    return payload


def get_prepared_response(status_code, prepared_body):
    response = {
        'statusCode': status_code,
        'headers': {
            'Content-Type': _CONTENT_TYPE,
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps(prepared_body)
    }
    return response

def decrypt_token(token):
    logger.info('Ingresó a función de desencriptar token')
    decrypt_data = None
    try:
        decode_base64 = base64.b64decode(token)
        cipher2 = AES.new(KEY_DECODE, AES_MODE, IV_DECODE)
        decrypt = unpad(cipher2.decrypt(decode_base64), 16)
        decrypt_data = decrypt.decode('UTF-8')
    except Exception as e:
        logger.info('Error al desencriptar token - {}', e)
    return decrypt_data

def get_bizagi_token():
    # obtener token
    url_token = '{}/Iniciativa/oauth2/server/token'.format(endpoints[environment]['server_bizagi'])
    logger.info("URL_GET_TOKEN - {}", url_token)
    response_token = requests.post(url_token, data='grant_type=client_credentials&scope=api', headers={'Content-Type': 'application/x-www-form-urlencoded'}, auth=HTTPBasicAuth(ClientID, ClientSecret))
    response_status_token = response_token.status_code
    logger.info("RESPONSE_STATUS_CODE_TOKEN - {}", response_status_token)
    response_json_token = response_token.json()

    if response_status_token != 200:
        prepared_body_token = get_prepared_body('post_project_participants', 'Error al obtener token de sesión', False)
        logger.info("RESPONSE_END_TOKEN - {}", get_prepared_response(400, prepared_body_token))
        return get_prepared_response(400, prepared_body_token)

    return get_prepared_response(200, response_json_token)


def get_list_projects_bizagi(bizagi_token, dsbl=False):
    size = 100
    page = 1
    repeat = True
    list_bizagi_projects = []
    # Se obtiene sólo los proyectos activos 
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_list_projects_bizagi = '{}/Iniciativa/odata/data/entities(9978487f-8368-4ace-98a5-55516e9df27c)/values'.format(endpoints[environment]['server_bizagi'])
        skip = (page*size) - size
        param = {_SIZE: size, _SKIP: skip}
        response_bizagi_projects = requests.get(url_list_projects_bizagi, params=param, headers={'Content-type': _CONTENT_TYPE, 'Authorization': f'Bearer {bizagi_token}'})
        response_json_list_projects = response_bizagi_projects.json()
        list_projec = response_json_list_projects['value']
        if _NEXT_LINK not in response_json_list_projects:
            repeat = False
        if dsbl == False:
            for x in list_projec:
                parameters = x['parameters']
                for i in parameters:
                    if i['xpath'] == 'dsbl' and i['value'] == False:
                        list_bizagi_projects.append(x)
        else:
            for x in list_projec:
                list_bizagi_projects.append(x)
        page += 1

    # Se mapeanlos campos a consumir
    projects_filters = []
    for x in list_bizagi_projects:
        parameters = x['parameters']
        projects_bizagi = {
            'id_bizagi': x['id'],
            'id_jira': None,
            'key_jira': None,
            'name_jira': None
        }
        for i in parameters:
            if i['xpath'] == 'sIddeproyecto':
                projects_bizagi['id_jira'] = i['value']
            if i['xpath'] == 'sCodigodeproyecto':
                projects_bizagi['key_jira'] = i['value']
            if i['xpath'] == 'sNombredeproyecto':
                projects_bizagi['name_jira'] = i['value']
        projects_filters.append(projects_bizagi)

    return projects_filters


def get_iduser_participants(accound_id, token):
    code_participant = None
    size = 100
    page = 1
    repeat = True
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_idparticipant = '{}/Iniciativa/odata/data/entities(68007f3b-7158-4c42-bab0-3b9897b00526)/values'.format(endpoints[environment]['server_bizagi'])
        skip = (page*size) - size
        payload = {_SIZE: size, _SKIP: skip}
        header = {'Content-Type': _CONTENT_TYPE,'Authorization': f'Bearer {token}'}
        response_bizagi_participants = requests.get(url_idparticipant, params=payload, headers=header)
        json_list_participants = response_bizagi_participants.json()

        if _NEXT_LINK not in json_list_participants:
            repeat = False

        for x in json_list_participants['value']:
            parameters = x['parameters']
            try:
                for i in parameters:
                    if (i['xpath'] == 'sCodigoParticipante') and (i['value'] == accound_id):
                        code_participant = x['id']
                        return code_participant
            except Exception as e:
                logger.info("No se encontro sCodigoParticipante - {}", e)
        page += 1

    return code_participant


def disable_participants_by_project(id_projectsparticipant, token):
    dsbl = False
    url_get_projectparticipant = "{}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values({})/update".format(endpoints[environment]['server_bizagi'], id_projectsparticipant)
    payload = json.dumps({
        "startParameters": [
            {
                "xpath": "dsbl",
                "value": "true"
            }
        ]
    })
    headers = {'Content-Type': _CONTENT_TYPE,'Authorization': f'Bearer {token}'}
    response = requests.request("POST", url_get_projectparticipant, headers=headers, data=payload)
    response_status = response.status_code
    if response_status == 200:
        return True
    return dsbl


def get_id_projectsparticipant_by_accoundid_and_projectid(token, accound_id, id_project_jira):
    id_projectsparticipant = None
    size = 100
    page = 1
    repeat = True
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_get_projectsparticipant = '{}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values'.format(endpoints[environment]['server_bizagi'])
        skip = (page*size) - size
        payload = {_SIZE: size, _SKIP: skip}
        header = {'Content-Type': _CONTENT_TYPE,'Authorization': f'Bearer {token}'}
        response_bizagi_projectsparticipant = requests.get(url_get_projectsparticipant, params=payload, headers=header)
        json_list_projectsparticipant = response_bizagi_projectsparticipant.json()

        if _NEXT_LINK not in json_list_projectsparticipant:
            repeat = False

        for x in json_list_projectsparticipant['value']:
            parameters = x['parameters']
            for i in parameters:
                if (i['xpath'] == _XPATH_CODE_PROJECT and i['value'] == id_project_jira):
                    for j in parameters:
                        if (j['xpath'] == _XPATH_CODE_PARTICIPANT and j['value'] == accound_id):
                            id_projectsparticipant = x['id']
                            return id_projectsparticipant
        page += 1

    return id_projectsparticipant

def post_projects_participants(list_participants_jira, id_project_bizagi, id_project_jira, token):
    users_add = []
    users_update = []
    users_register = {'id_project_jira': id_project_jira}
    # Obtener los participantes de la tabla intermedia proyectoParticipante de bizagi según el id de proyecto de jira
    bizagi_participants_by_project = get_list_participants_by_idproject(token, id_project_jira)
    logger.info('Participantes registrados en la entidad ParticipantesProyectos para el proyecto ->  {}: {}',id_project_jira, bizagi_participants_by_project)

    # Obtener los participantes de jira que no estén en la lista de ProyectoParticipante de bizagi, para agregarlo a la entidad ProyectoParticipante
    participants_not_in_bizagi = [item for item in list_participants_jira if item not in bizagi_participants_by_project]
    logger.info('Participantes registrados en Jira que no están en Bizagi para el proyecto "{}" - {}',id_project_jira, participants_not_in_bizagi)
    for i in participants_not_in_bizagi:
        # Buscar id de participante en Bizagi a partir de accoundId de participante de Jira
        id_participant_bizagi = get_iduser_participants(i, token)
        # validar que participante exista en la entidad listaParticipante de Bizagi
        if id_participant_bizagi != None:
            logger.info('Id de usuario de Jira encontrado en la entidad Participantes - "{}" ', i)
            url_project_participant = '{}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/create'.format(endpoints[environment]['server_bizagi'])
            header = {'Content-Type': _CONTENT_TYPE,'Authorization': f'Bearer {token}'}
            payload = json.dumps({
                "startParameters": [
                    {
                        "xpath": "ListadeProyectos",
                        "value": id_project_bizagi
                    },
                    {
                        "xpath": "ListadeParticipantes",
                        "value": id_participant_bizagi
                    },
                    {
                        "xpath": "dsbl",
                        "value": "false"
                    }
                ]
            })
            response_create = requests.request("POST", url_project_participant, headers=header, data=payload)
            status_create = response_create.status_code
            if status_create == 200:
                users_add.append(i)
    # Adicionar lista de participantes creados para un proyecto
    users_register['project_paricipant_create'] = len(users_add)
    # Obtener los participantes de bizagi que no estén en la lista de participantes de Jira, para proceder con su deshabilitación
    participants_not_in_jira = [item for item in bizagi_participants_by_project if item not in list_participants_jira]
    logger.info('Participantes registrados en Bizagi que no están en jira para el proyecto "{}" - {}',id_project_jira, participants_not_in_jira)
    
    for id_user_jira in participants_not_in_jira:
        id_projectsparticipant = get_id_projectsparticipant_by_accoundid_and_projectid(token, id_user_jira, id_project_jira)
        if id_projectsparticipant != None:
            response_dsbl = disable_participants_by_project(id_projectsparticipant, token)
            if response_dsbl == True:
                users_update.append(id_user_jira)
    # Adicionar lista de participantes actualizados(deshabilitar) para un proyecto
    users_register['project_paricipant_updated'] = len(users_update)
    return users_register


def get_list_participants_by_idproject(token, id_project_jira):
    participants = []
    size = 100
    page = 1
    repeat = True
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_idparticipant = '{}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values'.format(endpoints[environment]['server_bizagi'])
        skip = (page*size) - size
        payload = {_SIZE: size, _SKIP: skip}
        header = {'Content-Type': _CONTENT_TYPE,'Authorization': f'Bearer {token}'}
        response_bizagi_participants = requests.get(url_idparticipant, params=payload, headers=header)
        json_list_participants = response_bizagi_participants.json()
        if _NEXT_LINK not in json_list_participants:
            repeat = False

        for x in json_list_participants['value']:
            parameters = x['parameters']
            for i in parameters:
                if (i['xpath'] == _XPATH_CODE_PROJECT and i['value'] == id_project_jira):
                    for j in parameters:
                        if j['xpath'] == _XPATH_CODE_PARTICIPANT:
                            participants.append(j['value'])
        page += 1
    return participants

def get_list_id_projects_participants(bizagi_token, id_jira_project):
    size = 100
    page = 1
    repeat = True
    list_desabilitar=[]
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_list_projects_bizagi = '{}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values'.format(endpoints[environment]['server_bizagi'])
        skip = (page*size) - size
        param = {_SIZE: size, _SKIP: skip}
        response_bizagi_projects = requests.get(url_list_projects_bizagi, params=param, headers={'Content-type': _CONTENT_TYPE, 'Authorization': f'Bearer {bizagi_token}'})
        response_json_bizagi_projects = response_bizagi_projects.json()
        list_projecs = response_json_bizagi_projects['value']
        if _NEXT_LINK not in response_json_bizagi_projects:
            repeat = False
        for x in list_projecs:
            for y in x['parameters']:
                if (y['xpath'] == _XPATH_CODE_PROJECT and y['value'] == id_jira_project):
                    list_desabilitar.append(x['id'])
        page += 1

    return list_desabilitar


def get_participants_by_id_roles(url, token_user_jira):
    list_actors = []
    header = {'Accept': _CONTENT_TYPE}
    response_team = requests.request('GET', url, headers=header, auth=HTTPBasicAuth(USER_EMAIL, token_user_jira))
    response_json = response_team.json()
    # Obtener lista de actores
    actors = response_json['actors']
    if actors:
        for i in actors:
            list_actors.append(i['actorUser']['accountId'])

    return list_actors


def get_team_participants_jira_for_id(id_project, token_user_jira):
    team = []
    url_team = '{}/rest/api/3/project/{}/role'.format(endpoints[environment]['server_jira'], id_project)
    header = {'Accept': _CONTENT_TYPE}
    response_team = requests.request('GET', url_team, headers=header, auth=HTTPBasicAuth(USER_EMAIL, token_user_jira))
    response_json = response_team.json()
    logger.info('Roles para el proyecto - {}', response_json)
    link_roles = [v for k, v in response_json.items() if k in ['Team', 'Jefe de Proyecto', 'Scrum Master']]
    for l in link_roles:
        actors = get_participants_by_id_roles(l, token_user_jira)
        for t in actors:
            if t not in team:
                team.append(t)
    return team

def post_projects_by_participants(token, token_user_jira):
    # Variable
    msg_registers = []

    # Obtener proyectos de la lista de proyectos de Bizagi
    list_projecs_bizagi = get_list_projects_bizagi(token)
    logger.info('Listado de proyectos de Bizagi - {}', list_projecs_bizagi)
    for i in list_projecs_bizagi:
        id_project_jira = i['id_jira']
        id_project_bizagi = i['id_bizagi']
        logger.info('Id Jira - {}', id_project_jira)
        logger.info('Id Bizagi - {}', id_project_bizagi)
        list_participants = get_team_participants_jira_for_id(id_project_jira, token_user_jira)
        logger.info('Lista de Id de participantes de Jira - {}',list_participants)
        if list_participants:
            msg_rpta = post_projects_participants(list_participants, id_project_bizagi, id_project_jira, token)
            if msg_rpta:
                msg_registers.append(msg_rpta)

    prepared_body = get_prepared_body('post_project_participants', 'lista obtenida de registro de participantes por proyecto', True, data=msg_registers, code='00')
    logger.info("RESPONSE_END_POST_PROJECT_PARTICIPANTS - {}", prepared_body)
    return get_prepared_response(200, prepared_body)

def get_list_projects_jira(token_user_jira):
	list_group_user = get_user_from_group_jira(token_user_jira)
	page = 1
	size = 50
	list_jira_projects = []
	repeat = True
	while repeat:
		url_list_projects_jira = '{}/rest/api/3/project/search'.format(endpoints[environment]['server_jira'])
		#Api para consultar la categoría - GET /rest/api/3/projectCategory
		startat=(size*page) - size
		param = {'startAt': startat, 'maxResults': size, 'typeKey': 'software','categoryId': '10002', 'expand': 'lead', 'status': 'live'}
		response_jira_projects = requests.get(url_list_projects_jira, params= param ,headers={'Accept': _CONTENT_TYPE}, auth=HTTPBasicAuth(USER_EMAIL, token_user_jira))
		response_json_list_projects = response_jira_projects.json()
		if response_json_list_projects['isLast'] == True:
			repeat = False
		for x in response_json_list_projects['values']:
			print(x['lead']['accountId'], type(x['lead']['accountId']))
			if x['lead']['accountId'] in list_group_user:
				projects_jira = {
					'id': None,
					'key': None,
					'name': None
				}
				try:
					projects_jira['id'] = x['id']
				except Exception as e:
					logger.info("No se encontro id del proyecto - {}",e)
				try:
					projects_jira['key'] = x['key']
				except Exception as e:
					logger.info("No se encontro key del proyecto - {}",e)
				try:
					projects_jira['name'] = x['name']
				except Exception as e:
					logger.info("No se encontro name del proyecto - {}",e)
				list_jira_projects.append(projects_jira)
		page += 1

	return list_jira_projects


def get_user_from_group_jira(token_user_jira):
	page = 1
	size = 100
	list_group_user= []
	repeat = True
	while repeat:
		startat=(size*page) - size
		url_team = '{}/rest/api/3/group/member'.format(endpoints[environment]['server_jira'])
		header = {'Accept': _CONTENT_TYPE}
		param = {'startAt': startat, 'maxResults': size, 'groupname': 'jira-development'}
		response_team = requests.request('GET', url_team, params=param, headers= header, auth=HTTPBasicAuth(USER_EMAIL, token_user_jira))
		response_json_team = response_team.json()
		if response_json_team['isLast'] == True:
			repeat = False
		for x in response_json_team['values']:
			try:
				list_group_user.append(x['accountId'])
			except Exception as e:
				logger.info("No se encontro accountId del proyecto - {}",e)
		page += 1
	return list_group_user


def update_project(id_bizagi,token,nombre,codigo, dsbl = 'false'):
    url = "{}/Iniciativa/odata/data/entities(9978487f-8368-4ace-98a5-55516e9df27c)/values({})/update".format(endpoints[environment]['server_bizagi'] ,id_bizagi)

    payload = json.dumps({
        "startParameters": [
            {
                "xpath": "sNombredeproyecto",
                "value": nombre
            },
            {
                "xpath": "sCodigodeproyecto",
                "value": codigo
            },
            {
                "xpath": "dsbl",
                "value": dsbl
            }
        ]
    })
    headers = {
    'Content-Type': _CONTENT_TYPE,
    'Authorization': f'Bearer {token}'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    status_response = response.status_code
    logger.info('RESPONSE_JSON_UPDATE_PROJECT - {}', response.json())
    if status_response != 200:
        return False
    return True
    
def create_project_in_bizagi(key,nombre,id,token):
    url_create_project = "{}/Iniciativa/odata/data/entities(9978487f-8368-4ace-98a5-55516e9df27c)/create".format(endpoints[environment]['server_bizagi'])
    payload = json.dumps({
    "startParameters": [
    {
        "xpath": "sNombredeproyecto",
        "value": nombre
    },
    {
        "xpath": "sCodigodeproyecto",
        "value": key
    },
    {
        "xpath": "sIddeproyecto",
        "value": id
    },
    {
        "xpath": "dsbl",
        "value": "false"
    }
    ]
    })
    headers = {'Content-Type': _CONTENT_TYPE,'Authorization': f'Bearer {token}'}
    response_create_project = requests.request("POST", url_create_project, headers=headers, data=payload)
    status_code_create_project = response_create_project.status_code
    logger.info('RESPONSE_JSON_CREATE_PROJECT - {}', response_create_project.json())
    if status_code_create_project != 200:
        logger.info('Error al agregar el proyecto: - {}', nombre)
        return False
    return True



def disable_project(id_bizagi,token):
    url = "{}/Iniciativa/odata/data/entities(9978487f-8368-4ace-98a5-55516e9df27c)/values({})/update".format(endpoints[environment]['server_bizagi'], id_bizagi)

    payload = json.dumps({
    "startParameters": [
            {
            "xpath": "dsbl",
            "value": "true"
            }
        ]
    })
    headers = {'Content-Type': _CONTENT_TYPE, 'Authorization': f'Bearer {token}'}
    response = requests.request("POST", url, headers=headers, data=payload)
    status_code = response.status_code
    logger.info('RESPONSE_JSON_DISABLE_PROJECT - {}', response.json())
    if status_code != 200:
        return False
    return True 

def disable_participant_project(id_project_participant,token,dsbl='true'):
    url = "{}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values({})/update".format(endpoints[environment]['server_bizagi'], id_project_participant)

    payload = json.dumps({
    "startParameters": [
            {
                "xpath": "dsbl",
                "value": dsbl
            }
        ]
    })
    headers = {
    'Content-Type': _CONTENT_TYPE,
    'Authorization': f'Bearer {token}'
    }
    response_dslb = requests.request("POST", url, headers=headers, data=payload)
    status_code = response_dslb.status_code
    if status_code != 200:
        return False
    
    return True

 

def post_projects(token, token_user_jira,body):

    if not get_list_projects_bizagi(token):
        logger.info('Lista vacía en la entidad proyectos de Bizagi')
        response_body_create = []
        try:
            list_filter_projects_jira=get_list_projects_jira(token_user_jira)
            logger.info(' Cantidad de proyectos de Jira de categoría "Proyectos" - {}', len(list_filter_projects_jira))
            for i in list_filter_projects_jira:
                response_create = create_project_in_bizagi(i["key"],i["name"],i["id"],token)
                if response_create == True: response_body_create.append('Se creó correctamente el projecto "{}" '.format(i['name']))
            prepared_body=get_prepared_body("project_created", "lista de registro de projectos", True, data = response_body_create, errors={}, code='01')
            logger.info('RESPONSE_END_PROJECT_CREATED - {}', prepared_body)
            return get_prepared_response(200,prepared_body)
        except Exception as e:
            prepared_body=get_prepared_body("project_created", "Ocurrio un Error al crear el proyecto - {}".format(e), True, code='01')
            return get_prepared_response(400,prepared_body)
    else:
        #mapeo de campos
        webhook_events=["project_created","project_updated","project_deleted","project_archived","project_restored_archived"]
        if body["webhookEvent"] in webhook_events:
            id_jira=str(body['project']['id'])
            key=body['project']['key']
            name=body['project']['name']
            responsable=body['project']['projectLead']['accountId']
            # Cpturar el Evento del webhook 
            webhook_event = body['webhookEvent'].lower()
            logger.info('WEBHOOK_EVENT - {}', webhook_event)
    
            if webhook_event == "project_created":
                logger.info('Ingresó a evento de Creación de proyecto')
                # Obtener al responsable del proyecto
                print('Responsable de proyecto: ', responsable)
                list_ti_users=get_user_from_group_jira(token_user_jira)
                # Evaluar si el responsable pertenece a TI
                if responsable in list_ti_users:
                    response_create = create_project_in_bizagi(key,name,id_jira,token)
                    if response_create == False:
                        prepared_body=get_prepared_body("project_created", "Ocurrió un error al crear el proyecto {}".format(name), False, code='01')
                        return get_prepared_response(400, prepared_body)
                    prepared_body=get_prepared_body("project_created", "Se creó el proyecto {} correctamente".format(name), True, code='00')
                    return get_prepared_response(200,prepared_body)
                else:
                    prepared_body=get_prepared_body("project_created", "No se creó el proyecto {}, usuario no pertene a TI".format(name), False, code='01')
                    return get_prepared_response(400,prepared_body)
            if webhook_event == "project_archived":
                logger.info('Ingresó a evento de archivar el proyecto')
                id_bizagi=None
                len_participants = []
    
                list_bizagi_projects=get_list_projects_bizagi(token)
                for i in list_bizagi_projects:
                    if i['id_jira'] == id_jira:
                        id_bizagi=i['id_bizagi']
                # Valida la existencia del proyecto en la entidad proyectos de Bizagi
                if id_bizagi == None:
                    prepared_body=get_prepared_body("project_archived", "No se deshabilitó el proyecto {} porque no se encuentra registrado.".format(name), False, code='01')
                    logger.info('NOT_FOUND_PROJECT - {}', prepared_body)
                    return get_prepared_response(400,prepared_body)
                # Deshabilita el proyecto
                response_disable =update_project(id_bizagi,token,name,key,'true')# disable_project(id_bizagi,token)
                if response_disable == False:
                    prepared_body=get_prepared_body("disable_project", "Ocurrió un error al intentar deshabilitar el proyecto {}".format(name), False, code='01')
                    logger.info('ERROR_DISABLE_PROJECT - {}', prepared_body)
                    return get_prepared_response(400,prepared_body)
                # Obtiene lista de participantes para el proyecto a deshabilitar
                list_desabilitar=get_list_id_projects_participants(token, id_jira)
                logger.info('Se encontró {} participantes para el projecto {}'.format(len(list_desabilitar), name))
                for y in list_desabilitar:
                    # Se recorre la lista de participantes a deshabilitar por proyecto
                    response_disable = disable_participant_project(y,token)
                    if response_disable == True: len_participants.append(y)
                prepared_body=get_prepared_body("disable_project", "Se deshabilitó el proyecto {} y un total de {} participantes.".format(name ,len(len_participants)), True, code='00')
                logger.info('RESPONSE_DISABLE_PROJECT - {}', prepared_body)
                return get_prepared_response(200,prepared_body)

            # fin de archivado
            elif webhook_event == "project_restored_archived":
                logger.info('Ingresó a evento de restaurar el proyecto')
				#si no esta debe agregarse 
				#si esta  debe activarse
				#Buscar proyecto en bizagi
                id_bizagi=None
                id_project = None
                # Obtiene los proyectos habilitados y deshabilitados en la entidad lista de proyectos de bizagi
                list_bizagi_projects=get_list_projects_bizagi(token, dsbl= True)
                print('list_bizagi_projects ----> ', list_bizagi_projects)
                for i in list_bizagi_projects:
                    if i['id_jira'] ==id_jira:
                        id_project = i['id_jira']
                        id_bizagi = i['id_bizagi']
                list_ti_users=get_user_from_group_jira(token_user_jira)
                if id_bizagi != None:
                    print('Existe el proyecto en Bizagi: ', id_project)
                    list_desabilitar=get_list_id_projects_participants(token, id_jira)
                    logger.info('El proyecto {} tiene registrado {} participantes', id_jira, len(list_desabilitar))
                    if responsable in list_ti_users:
                        logger.info('El responsable pertenece a TI')
                        response_update = update_project(id_bizagi,token,name,key)#activa proyecto y actualiza
                        if response_update == False:
                            prepared_body=get_prepared_body("project_restored_archived", f"Error al intentar restaurar el proyecto con Id {id_project}", False, code='00')
                            return get_prepared_response(400,prepared_body)
                        for y in list_desabilitar:
                            # Se recorre la lista de participantes a deshabilitar por proyecto
                            response_disable = disable_participant_project(y,token,'false') # se activa los proyectos
                            logger.info("reponse_disable",response_disable)
                        
                        prepared_body=get_prepared_body("project_restored_archived", f"Se restauro el proyecto {id_project}", True, code='01')
                        return get_prepared_response(200,prepared_body)
                    else:
                        logger.info('El responsable no pertenece a TI')
                        response_disable_proyect=update_project(id_bizagi,token,name,key,'true')#disable_project(id_bizagi,token)
                        if response_disable_proyect == False:
                            logger.info('Ocurrión un error al restaurar el proyecto - {}', id_project)
                        
                        for y in list_desabilitar:
                        # Se recorre la lista de participantes a deshabilitar por proyecto
                            response_disable = disable_participant_project(y,token)
                            logger.info("response_disable -{}",response_disable)
                        prepared_body=get_prepared_body("project_restored_archived", f"Se desabilito el proyecto {id_project}", True, code='01')
                        return get_prepared_response(200,prepared_body)
                        
                else:
                    print('No existe el proyecto en Bizagi: ', id_jira)
                    account_lead=body['project']['projectLead']['accountId']
                    # Si el usuario no pertenece a TI
                    if account_lead not in list_ti_users:
                        print('El usuario con id {} no pertenece al grupo de TI'.format(account_lead))
                        prepared_body=get_prepared_body("project_restored_archived", "El usuario no pertenece al grupo de TI", True, code='01')
                        return get_prepared_response(200,prepared_body)
    
                    response_create_project = create_project_in_bizagi(key,name,id_jira,token)
                    if response_create_project == False:
                        prepared_body=get_prepared_body("project_restored_archived", "Ocurrió un error al intentar registrar el proyecto {}".format(name), False, code='01')
                        return get_prepared_response(400,prepared_body)
                    prepared_body=get_prepared_body("project_restored_archived", "Se han registrado el proyecto {} Correctamente".format(name), True, code='00')
                    return get_prepared_response(200,prepared_body)
                
            elif webhook_event == "project_updated":
                logger.info('Ingresó al evento de actualización')
                #buscar en la lista de proyectos sin no existe actualizo
                id_bizagi=None
                id_project = None
    
                list_bizagi_projects=get_list_projects_bizagi(token)
                for i in list_bizagi_projects:
                    if i['id_jira'] ==id_jira:
                        id_project = i['id_jira']
                        id_bizagi = i['id_bizagi']
                
                list_ti_users=get_user_from_group_jira(token_user_jira)
                if id_bizagi != None:
                    print('Existe el proyecto en Bizagi: ', id_project)
                    list_desabilitar=get_list_id_projects_participants(token, id_jira)
                    logger.info('El proyecto {} tiene registrado {} participantes', id_jira, len(list_desabilitar))
                    if responsable in list_ti_users:
                        logger.info('El responsable pertenece a TI')
                        response_update = update_project(id_bizagi,token,name,key)#activa proyecto y actualiza
                        if response_update == False:
                            prepared_body=get_prepared_body("project_updated", f"Error al intentar actualizar el proyecto con Id {id_project}", False, code='00')
                            return get_prepared_response(400,prepared_body)
                        for y in list_desabilitar:
                            # Se recorre la lista de participantes a deshabilitar por proyecto
                            response_disable = disable_participant_project(y,token,'false') # se desabilita los proyectos
                            logger.info("reponse_disable",response_disable)
                        
                        prepared_body=get_prepared_body("project_updated", f"Se actualizó el proyecto {id_project}", True, code='01')
                        return get_prepared_response(200,prepared_body)
                    else:
                        logger.info('El responsable no pertenece a TI')
                        response_disable_proyect=update_project(id_bizagi,token,name,key,'true')#disable_project(id_bizagi,token)
                        if response_disable_proyect == False:
                            logger.info('Ocurrión un error al actualizar el proyecto - {}', id_project)
                        
                        for y in list_desabilitar:
                        # Se recorre la lista de participantes a deshabilitar por proyecto
                            response_disable = disable_participant_project(y,token)
                            logger.info("response_disable -{}",response_disable)
                        prepared_body=get_prepared_body("project_updated", f"Se actualizó el proyecto {id_project}", True, code='01')
                        return get_prepared_response(200,prepared_body)
                        
                else:
                    print('No existe el proyecto en Bizagi: ', id_jira)
                    account_lead=body['project']['projectLead']['accountId']
                    # Si el usuario no pertenece a TI
                    if account_lead not in list_ti_users:
                        print('El usuario con id {} no pertenece al grupo de TI'.format(account_lead))
                        prepared_body=get_prepared_body("project_updated", "El usuario no pertenece al grupo de TI", True, code='01')
                        return get_prepared_response(200,prepared_body)
    
                    response_create_project = create_project_in_bizagi(key,name,id_jira,token)
                    if response_create_project == False:
                        prepared_body=get_prepared_body("project_updated", "Ocurrió un error al intentar registrar el proyecto {}".format(name), False, code='01')
                        return get_prepared_response(400,prepared_body)
                    prepared_body=get_prepared_body("project_updated", "Se han registrado el proyecto {} Correctamente".format(name), True, code='00')
                    return get_prepared_response(200,prepared_body)
    
            elif webhook_event == "project_deleted": #project_soft_deleted proyecto al tacho aun se puede restaurar 60 dias
                id_bizagi=None
                len_participants = []
    
                list_bizagi_projects=get_list_projects_bizagi(token)
                for i in list_bizagi_projects:
                    if i['id_jira'] == id_jira:
                        id_bizagi=i['id_bizagi']
                # Valida la existencia del proyecto en la entidad proyectos de Bizagi
                if id_bizagi == None:
                    prepared_body=get_prepared_body("disable_project", "No se deshabilitó el proyecto {} porque no se encuentra registrado.".format(name), False, code='01')
                    logger.info('NOT_FOUND_PROJECT - {}', prepared_body)
                    return get_prepared_response(400,prepared_body)
                # Deshabilita el proyecto
                response_disable =update_project(id_bizagi,token,name,key,'true')# disable_project(id_bizagi,token)
                if response_disable == False:
                    prepared_body=get_prepared_body("disable_project", "Ocurrió un error al intentar deshabilitar el proyecto {}".format(name), False, code='01')
                    logger.info('ERROR_DISABLE_PROJECT - {}', prepared_body)
                    return get_prepared_response(400,prepared_body)
                # Obtiene lista de participantes para el proyecto a deshabilitar
                list_desabilitar=get_list_id_projects_participants(token, id_jira)
                logger.info('Se encontró {} participantes para el projecto {}'.format(len(list_desabilitar), name))
                for y in list_desabilitar:
                    # Se recorre la lista de participantes a deshabilitar por proyecto
                    response_disable = disable_participant_project(y,token)
                    if response_disable == True: len_participants.append(y)
                prepared_body=get_prepared_body("disable_project", "Se deshabilitó el proyecto {} y un total de {} participantes.".format(name ,len(len_participants)), True, code='00')
                logger.info('RESPONSE_DISABLE_PROJECT - {}', prepared_body)
                return get_prepared_response(200,prepared_body)

def handler(event, context):
    
    logger.info("AMBIENTE DE DESPLIEGUE - {}", environment)
    logger.info('Event - {}', json.dumps(event))
    
    # Obtener Token de Bizagi
    response_bizagi_token = get_bizagi_token()
    if response_bizagi_token['statusCode'] != 200:
        logger.info(response_bizagi_token)
        return response_bizagi_token
    json_body = json.loads(response_bizagi_token['body'])
    token_user_bizagi = json_body['access_token']
    
    # Desencryptar token de usuario para consultas en Jira
    token_user_jira = decrypt_token(USER_TOKEN)

    if 'body'in event and event['body']:
        body = json.loads(event['body'])
        logger.info('Se envió body desde el webhook, se ejecutará función de creación de projecto')
        response_body = post_projects(token_user_bizagi, token_user_jira,body)
        logger.info('RESPONDE_END_PROJECTS - {}', response_body)
        return response_body
    else:
        logger.info('No se envió body, se ejecutará función de creación de participantes por projecto')
        response_body = post_projects_by_participants(token_user_bizagi, token_user_jira)
        logger.info('RESPONDE_END_PARTICIPANTS - {}', response_body)
        return response_body

